# Wrap C++ Classes into C Code

The tiny program demonstrates how to wrap C++ classes into C code.

## System Requirements

* C++ compiler (Clang or GCC)
* GNU Make (for compilation only)

## Usage

Clone the project:

```
$ git clone https://github.com/cwchentw/wrap-cpp-into-c.git
```

Move your working directory to the root of the cloned repo:

```
$ cd wrap-cpp-into-c
```

Compile the application:

```
$ make LDFLAGS=-lm
```

Run the compiled program:

```
$ ./dist/program && echo $?
0
```

## Copyright

Copyright (c) 2021 Michelle Chen. Licensed under MIT.
