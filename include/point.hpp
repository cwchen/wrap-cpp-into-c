/* point.hpp */
#pragma once

class Point
{
public:
    Point() : Point(0.0, 0.0) {};
    Point(double x, double y);
    double x();
    void setX(double x);
    double y();
    void setY(double y);
    static double distanceBetween(Point *p, Point *q);
private:
    double _x;
    double _y;
};