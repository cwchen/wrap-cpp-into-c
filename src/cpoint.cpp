/* cpoint.cpp */
#include <cassert>
#include <cstdlib>
#include "point.hpp"
#include "point.h"

struct point_t {
    Point *pt;
};

point_t * point_zero(void)
{
    return point_new(0.0, 0.0);
}

point_t * point_new(double x, double y)
{
    point_t *self = \
        (point_t *) malloc(sizeof(point_t));
    if (!self)
        return self;

    self->pt = new Point(x, y);
    if (!(self->pt)) {
        free(self);
        self = NULL;
        return self;
    }

    return self;
}

void point_delete(void *self)
{
    if (!self)
        return;

    Point *pt = ((point_t *)self)->pt;
    if (pt)
        delete pt;

    free(self);
}

double point_x(point_t *self)
{
    assert(self && (self->pt));

    return self->pt->x();
}

void point_set_x(point_t *self, double x)
{
    assert(self && (self->pt));

    self->pt->setX(x);
}

double point_y(point_t *self)
{
    assert(self && (self->pt));

    return self->pt->y();
}

void point_set_y(point_t *self, double y)
{
    assert(self && (self->pt));

    self->pt->setY(y);
}

double point_distance_between(point_t *p, point_t *q)
{
    assert(p && (p->pt));
    assert(q && (q->pt));

    return Point::distanceBetween(p->pt, q->pt);
}