/* point.cpp */
#include <cmath>
#include "point.hpp"


Point::Point(double x, double y)
{
    setX(x);
    setY(y);
}

double Point::x()
{
    return _x;
}

void Point::setX(double x)
{
    _x = x;
}

double Point::y()
{
    return _y;
}

void Point::setY(double y)
{
    _y = y;
}

double Point::distanceBetween(Point *p, Point *q)
{
    double dx = p->x() - q->x();
    double dy = p->y() - q->y();

    return sqrt(dx * dx + dy * dy);
}